"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Transport = require("winston-transport");
const util_1 = require("./util");
class WebDebug extends Transport {
    constructor(opts) {
        super(opts);
        this.sockets = new util_1.ArrayList();
    }
    log(info, callback = null) {
        setImmediate(() => {
            this.emit('logged', info);
            this.notifySockets("log", info);
        });
        if (callback)
            callback();
    }
    registerSocket(socket) {
        console.log("Registering socket...");
        if (!this.sockets.contains(socket)) {
            this.sockets.add(socket);
            socket.on("debug unregister", () => {
                // Sends feedback if socket could not be removed
                if (!this.sockets.remove(socket)) {
                    console.info("Sockets could not be removed from a WebDebugger");
                }
            });
            console.log("Socket registered for WebDebug");
        }
    }
    notifySockets(tag, data) {
        this.sockets.array.forEach(socket => {
            socket.emit("debug " + tag, data);
        });
    }
    notifySocketsDiagnostic(tag, data) {
        this.notifySockets(tag, data.diagnostic());
    }
}
exports.WebDebug = WebDebug;
//# sourceMappingURL=webdebug.js.map