"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Queue {
    constructor(run, maxTasks = 1, retryOnFailure = true) {
        this.run = run;
        this.maxTasks = maxTasks;
        this.retryOnFailure = retryOnFailure;
        this.queue = [];
        this.tasksRunning = 0;
    }
    next() {
        if (this.maxTasks <= this.tasksRunning || this.queue.length == 0) {
            return;
        }
        var pRes = this.queue[0];
        pRes.ready = true;
        this.tasksRunning++;
        pRes.run();
        this.queue = this.queue.slice(1, this.queue.length);
    }
    add(value) {
        var res = new PendingResult(this, value, this.run);
        this.queue.push(res);
        this.next();
        return res;
    }
    continueQueue() {
    }
    pauseQueue() {
    }
}
exports.Queue = Queue;
class PendingResult {
    constructor(queue, value, runFunc) {
        this.queue = queue;
        this.value = value;
        this.runFunc = runFunc;
        this._canceledFunc = (v) => { console.log("Test " + v); };
        this.result = null;
        this.rejected = true;
        this.ready = false;
        this.executed = false;
    }
    next(result) {
        if (this.resultFunc != null) {
            this.resultFunc(result);
        }
    }
    then(func) {
        this._resultFunc = func;
        this.resultFunc = (r) => {
            func(r);
            this.queue.tasksRunning--,
                this.queue.next();
        };
        return this;
    }
    canceled(func) {
        this._canceledFunc = func;
        this.canceledFunc = (v) => {
            console.log("Canceled");
            this.rejected = true;
            func(v);
            if (this.queue.retryOnFailure) {
                var clone = this.cleanClone();
                clone.ready = true;
                clone.run();
            }
            else {
                this.queue.tasksRunning--,
                    this.queue.next();
            }
        };
        return this;
    }
    run() {
        if (this.resultFunc == null || !this.ready || this.executed) {
            return;
        }
        this.runFunc(this.value, (v) => {
            this.result = v;
            this.resultFunc(v);
        }, this.canceledFunc);
    }
    cleanClone() {
        var pRes = new PendingResult(this.queue, this.value, this.runFunc);
        pRes.then(this._resultFunc).canceled(this._canceledFunc);
        return pRes;
    }
}
exports.PendingResult = PendingResult;
//# sourceMappingURL=queue.js.map