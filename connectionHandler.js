"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("./util");
const Subject_1 = require("rxjs/Subject");
const winston = require("winston");
const { combine, timestamp, label, printf } = winston.format;
exports.logger = null;
class ConnectionHandler {
    constructor(server, auth, logFormat, webDebug) {
        this.server = server;
        this.auth = auth;
        this.logFormat = logFormat;
        this.webDebug = webDebug;
        this._onconnection = new Subject_1.Subject();
        /**
         * @description Key: Auth-Token, Value: Connection
         */
        this.connections = new util_1.HashMap();
        exports.logger = winston.createLogger({
            format: combine(label({ label: 'CON' }), timestamp(), logFormat),
            transports: [
                new winston.transports.Console({
                    handleExceptions: true
                }),
                new winston.transports.File({
                    filename: "logs.log",
                    handleExceptions: true
                }),
                webDebug
            ],
            exitOnError: false
        });
        server.on("connection", (socket) => {
            if (socket == null) {
                return;
            }
            socket.on("debug register", (data) => {
                webDebug.registerSocket(socket);
                for (let entry of this.connections.map) {
                    socket.emit("debug connection register", entry.value.diagnostic());
                }
            });
            // socket.on("online-log-con", () => {
            //     console.log("Added socket to logger")
            //     log.addSocket(socket);
            // }); 
            exports.logger.info("Socket connected");
            socket.on("token", token => {
                exports.logger.info("Socket tries to reconnect with token");
                // Check, if user is still logged in
                this.auth.isLoggedIn(token).then(resp => {
                    if (resp.success) {
                        var con = this.getConnectionWithOldSocket(token);
                        if (con != null) {
                            exports.logger.info("Old connection found");
                            con.connectSocket(socket);
                        }
                        else {
                            exports.logger.info("No old connection found!");
                            this.auth.logout(token);
                            socket.emit("login", { success: false });
                            socket.emit("rejected");
                        }
                    }
                    else {
                        exports.logger.info("Socket reconnect rejected");
                        socket.emit("login", { success: false });
                        socket.emit("rejected");
                    }
                });
            });
            socket.on("login", data => {
                exports.logger.info("User tries to log in");
                exports.logger.info("Socket tries to login");
                if (data.guest) {
                    auth.guest(data.username).then(token => {
                        exports.logger.info("Creating new connection... : " + data.username);
                        var con = this.createConnection(socket, token, data.username);
                        exports.logger.info("New Connection created: " + data.username);
                        webDebug.notifySockets("connection register", con.diagnostic());
                        con.on("destroy", () => {
                            webDebug.notifySockets("connection destroy", con.token);
                        });
                        con.token = token;
                        con.emit("login", { success: true, token: token });
                    }).canceled(initVal => {
                        exports.logger.info("Duplicated Names!");
                        socket.emit("err", { code: 3, message: "This username is already in use!" });
                        exports.logger.info("Socket tried to login in with an already existing name");
                    }).run();
                }
                else {
                    auth.login(data.username, data.password);
                }
            });
        });
    }
    on(event) {
        switch (event) {
            case "connection": {
                return this._onconnection;
            }
        }
    }
    createConnection(socket, token, nickname) {
        var con = new Connection(socket, this.webDebug);
        con.token = token;
        con.nickname = nickname;
        this._onconnection.next(con);
        con.on("disconnect", () => {
            exports.logger.info("Client disconnects");
            con.disconnectSocket();
        });
        con.on("destroy", () => {
            this.auth.logout(token);
            this.removeConnection(con);
        });
        // Ping Pong
        // con.startPinging();
        con.connectionLost.subscribe(() => {
            con.updateWebDebugStatus();
        });
        con.reconnected.subscribe(() => {
            con.updateWebDebugStatus();
        });
        this.connections.push(token, con);
        console.log("Test");
        return con;
    }
    getConnectionWithOldSocket(token) {
        return this.connections.get(token);
    }
    removeConnection(connection) {
        this.connections.removeValue(connection);
    }
}
exports.ConnectionHandler = ConnectionHandler;
class Connection {
    constructor(socket, webDebug) {
        this.socket = socket;
        this.webDebug = webDebug;
        this.TIMEOUT = 20000;
        this.TIMEOUT_PING = 25000;
        this.reconnected = new Subject_1.Subject();
        this.connectionLost = new Subject_1.Subject();
        this._destroySubject = new Subject_1.Subject();
        this.events = new util_1.MulitHashMap();
        this.subscriptions = new util_1.ArrayList();
        this.connected = true;
        this.lastKnownID = null;
        this.address = null;
        this.token = null;
        this.nickname = null;
        this.currentGame = null;
        this.timeout = null;
        this.pinging = null;
        this.pingingIndex = -1;
        this.disconnected = true;
        this.lastKnownID = socket.id;
        this.address = socket.handshake.address;
        // this.reconnected.next(socket);
    }
    /**
     *
     * @description Inserts the socket into the connection and attaches previously used events to it.
     */
    connectSocket(socket) {
        if (this.connected) {
            return;
        }
        exports.logger.info("Client reconnects");
        clearTimeout(this.timeout);
        this.timeout = null;
        this.socket = socket;
        this.connected = true;
        for (let event of this.events.map) {
            for (let action of event.value) {
                // logger.info(action);
                this.socket.on(event.key, data => {
                    if (data == null || data == undefined) {
                        action();
                    }
                    else {
                        action(data);
                    }
                });
            }
        }
        this.socket.emit("login", { success: true, token: this.token });
        this.reconnected.next(socket);
    }
    // startPinging(){
    //     if(this.pinging != null)return;
    //     this.pinging = setInterval(() => {
    //         this.ping();
    //     }, 7500);
    //     this.pingingIndex = this.on("car pong", () => {
    //         clearTimeout(this.timeout);
    //     });
    // }
    // stopPinging(){
    //     if(this.pinging == null)return;
    //     clearInterval(this.pinging);
    //     this.pinging = null;
    //     this.unsubscribe("pong", this.pingingIndex);
    // }
    ping() {
        this.emit("ping");
        this.timeout = setTimeout(() => {
            this.disconnectSocket();
        }, this.TIMEOUT_PING);
    }
    /**
     *
     * @description Used to manualy unsubscribe a listener
     */
    unsubscribe(event, id) {
        this.events.removeItemAt(event, id);
    }
    /**
     *
     * @description Returns an observable that is triggered when the socket event is being called
     */
    on(event, action) {
        switch (event) {
            // Reserved event
            case "reconnect": {
                this.reconnected.subscribe((socket) => {
                    action(socket);
                });
                break;
            }
            case "destroy": {
                this._destroySubject.subscribe(() => {
                    action();
                });
                break;
            }
            default: {
                this.events.push(event, action);
                this.socket.on(event, data => {
                    if (data == null || data == undefined) {
                        action();
                    }
                    else {
                        action(data);
                    }
                });
            }
        }
        return this.events.size() - 1;
    }
    emit(event, data = null) {
        if (this.connected) {
            try {
                if (data == null) {
                    this.socket.emit(event);
                }
                else {
                    this.socket.emit(event, data);
                }
            }
            catch (e) {
                exports.logger.error(e);
            }
        }
    }
    // Just disconnects the socket, data will be saved for 30 min;
    disconnectSocket() {
        this.connected = false;
        exports.logger.info("Socket disconnected! Tyring to timeout.");
        // this.socket = null;
        this.updateWebDebugStatus();
        this.connectionLost.next();
        this.timeout = setTimeout(() => {
            this.destroyConnection();
        }, this.TIMEOUT);
    }
    // Destroyes socket connection and data
    destroyConnection() {
        exports.logger.info("Timeout! Destroying connection.");
        this._destroySubject.next(null);
        clearTimeout(this.timeout);
        this.timeout = null;
        for (let subscription of this.subscriptions) {
            subscription.unsubscribe();
        }
    }
    onConnected(action) {
        action();
    }
    diagnostic() {
        return {
            token: this.token,
            username: this.nickname,
            address: this.socket.conn.remoteAddress,
            state: this.socket.conn.readyState,
            currentGame: this.currentGame
        };
    }
    updateWebDebugStatus() {
        console.log("Updating web debug status");
        this.webDebug.notifySockets("connection update", this.diagnostic());
    }
}
exports.Connection = Connection;
class Address {
    constructor(address, port) {
        this.address = address;
        this.port = port;
    }
}
exports.Address = Address;
//# sourceMappingURL=connectionHandler.js.map