
import { Server, Socket } from "socket.io";
import { HashMap, ArrayList, MulitHashMap } from "./util";
import { Logger } from "winston";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { Auth } from "./auth";
import { Subscription } from "rxjs/Subscription";
import { ConnectionLogger } from "./onlinelog";
import { WebDebug, Diagnosticable } from "./webdebug";
import * as winston from "winston";

const { combine, timestamp, label, printf } = winston.format;

export var logger = null;


export class ConnectionHandler{

    _onconnection: Subject<Connection> = new Subject<Connection>(); 

    /** 
     * @description Key: Auth-Token, Value: Connection
     */
    private connections: HashMap<string, Connection> = new HashMap();

    constructor( private server: Server, private auth: Auth, public logFormat, public webDebug: WebDebug){
        logger = winston.createLogger({
            format: combine(
                label({ label: 'CON' }),
                timestamp(),
                logFormat
            ),
            transports: [
                new winston.transports.Console({
                    handleExceptions: true
                }),
                new winston.transports.File({
                    filename: "logs.log",
                    handleExceptions: true
                }),
                webDebug
            ],
            exitOnError: false
        });

        server.on("connection", (socket) => {

            if(socket == null){
                return;
            }

            socket.on("debug register", (data) => {
                webDebug.registerSocket(socket);

                for(let entry of this.connections.map){
                    socket.emit("debug connection register", entry.value.diagnostic())
                }

            });

            // socket.on("online-log-con", () => {
            //     console.log("Added socket to logger")
            //     log.addSocket(socket);
            // }); 

            logger.info("Socket connected");

            socket.on("token", token => {

                logger.info("Socket tries to reconnect with token");
                // Check, if user is still logged in
                this.auth.isLoggedIn(token).then(resp => {
                    if(resp.success){
                        var con = this.getConnectionWithOldSocket(token);

                        if(con != null){
                            logger.info("Old connection found");
                            con.connectSocket(socket);
                        }else{
                            logger.info("No old connection found!");
                            this.auth.logout(token);
                            socket.emit("login", {success: false});
                            socket.emit("rejected");
                        }

                    }else{
                        logger.info("Socket reconnect rejected");
                        socket.emit("login", {success: false});
                        socket.emit("rejected");
                    }
                });


            });

            socket.on("login", data => {
                logger.info("User tries to log in");
                logger.info("Socket tries to login");
                if(data.guest){

                    
                    auth.guest(data.username).then(token => {

                        
                        logger.info("Creating new connection... : " + data.username);
                        var con = this.createConnection(socket, token, data.username);
                        logger.info("New Connection created: " + data.username);

                        webDebug.notifySockets("connection register", con.diagnostic())

                        con.on("destroy", () => {
                            webDebug.notifySockets("connection destroy", con.token)
                        });
                        
                        con.token = token;
                        con.emit("login",  {success: true, token: token});

                    }).canceled(initVal => {
                        logger.info("Duplicated Names!");
                        socket.emit("err", {code: 3, message: "This username is already in use!"})
                        logger.info("Socket tried to login in with an already existing name");
                    }).run();

                    
                }else{
                    auth.login(data.username, data.password);
                }
            });

        });
    }


    on(event: string){
        switch(event){
            case "connection": {
                return this._onconnection;
            }
        }
    }

    createConnection(socket: Socket, token: string, nickname: string): Connection{
        var con = new Connection(socket, this.webDebug);
        con.token = token;
        con.nickname = nickname;

        this._onconnection.next(con);

        con.on("disconnect", () => {
            logger.info("Client disconnects");
            con.disconnectSocket();
        })
        
        con.on("destroy", () => {
            this.auth.logout(token);
            this.removeConnection(con);
        });


        // Ping Pong

        // con.startPinging();

        con.connectionLost.subscribe(() => {
            con.updateWebDebugStatus();
        });
        con.reconnected.subscribe(() => {
            con.updateWebDebugStatus();
        });


        this.connections.push(token, con);


        console.log("Test");
        return con;
    }

    

    getConnectionWithOldSocket(token): Connection{
        return this.connections.get(token);
    }

    public removeConnection(connection: Connection){
        this.connections.removeValue(connection);
    }
    

}


export class Connection implements Diagnosticable{

    public TIMEOUT: number = 20000;
    public TIMEOUT_PING: number = 25000;

    public reconnected: Subject<Socket> = new Subject<Socket>();
    public connectionLost: Subject<void> = new Subject<void>();
    private _destroySubject: Subject<void> = new Subject<void>();
 
    events: MulitHashMap<string, Function> = new MulitHashMap<string, Function>();

    subscriptions: ArrayList<Subscription> = new ArrayList<Subscription>();

    connected: boolean = true;
    lastKnownID: string = null;
    address: string = null;

    token: string = null;
    nickname: string = null;

    currentGame: string = null;

    timeout = null;
    pinging = null;
    pingingIndex: number = -1;

    disconnected: boolean = true;
    


    constructor(
        public socket: Socket,
        private webDebug: WebDebug
    ){
        this.lastKnownID = socket.id;
        this.address = socket.handshake.address;
        // this.reconnected.next(socket);
    }


    
    /**
     * 
     * @description Inserts the socket into the connection and attaches previously used events to it.
     */
    connectSocket(socket: Socket){
        if(this.connected){
            return;
        }
        logger.info("Client reconnects");
        clearTimeout(this.timeout);
        this.timeout = null;
        this.socket = socket;
        this.connected = true;
        
        for(let event of this.events.map){
            for(let action of event.value){
                // logger.info(action);
                this.socket.on(event.key, data => {
                    if(data == null || data == undefined){
                        action();
                    }else{
                        action(data);
                    }
                });
            }
        }

        this.socket.emit("login",  {success: true, token: this.token});

        this.reconnected.next(socket);



    }

    // startPinging(){
    //     if(this.pinging != null)return;
    //     this.pinging = setInterval(() => {
    //         this.ping();
    //     }, 7500);

    //     this.pingingIndex = this.on("car pong", () => {
    //         clearTimeout(this.timeout);
    //     });
    // }

    // stopPinging(){
    //     if(this.pinging == null)return;
    //     clearInterval(this.pinging);
    //     this.pinging = null;
    //     this.unsubscribe("pong", this.pingingIndex);
    // }

    ping(){
        this.emit("ping");
        
        this.timeout = setTimeout(() => {
            this.disconnectSocket();
        }, this.TIMEOUT_PING);

    }


    /**
     * 
     * @description Used to manualy unsubscribe a listener
     */
    unsubscribe(event:string, id: number){
        this.events.removeItemAt(event, id);
    }


    /**
     * 
     * @description Returns an observable that is triggered when the socket event is being called
     */
    on<T = any>(event: string, action: Function): number{


        switch(event){

            // Reserved event
            case "reconnect": {
                this.reconnected.subscribe((socket) => {
                    action(socket);
                });
                
                break;
            }
            case "destroy": {

                this._destroySubject.subscribe(() => {
                    action();
                });

                break;
            }
            default: {
                this.events.push(event, action);
                this.socket.on(event, data => {
                    if(data == null || data == undefined){
                        action();
                    }else{
                        action(data);
                    }
                });
            }
        }

        return this.events.size() - 1;
    }


    emit(event: string, data: any = null){
        if(this.connected){
            try{
                
                if(data == null){
                    this.socket.emit(event);
                }else{
                    this.socket.emit(event, data);
                }
            }catch(e){
                logger.error(e);
            }
        }
    }



    // Just disconnects the socket, data will be saved for 30 min;
    disconnectSocket(){
        this.connected = false;
        logger.info("Socket disconnected! Tyring to timeout.");
        // this.socket = null;
        this.updateWebDebugStatus();
        this.connectionLost.next();
        this.timeout = setTimeout(() => {
            this.destroyConnection();
        }, this.TIMEOUT);
    }

    // Destroyes socket connection and data
    destroyConnection(){
        logger.info("Timeout! Destroying connection.");
        this._destroySubject.next(null);
        clearTimeout(this.timeout);
        this.timeout = null;

        for(let subscription of this.subscriptions){
            subscription.unsubscribe();
        }

        

    }

    onConnected(action: any){
        action();
    }


    public diagnostic(): Object{
        return {
            token: this.token,
            username: this.nickname,
            address: this.socket.conn.remoteAddress,
            state: this.socket.conn.readyState,
            currentGame: this.currentGame
        }
    }

    public updateWebDebugStatus(){
        console.log("Updating web debug status");
        this.webDebug.notifySockets("connection update", this.diagnostic())
    }

}

export class Address{

    constructor(
        public address: string,
        public port 
    ){}

}