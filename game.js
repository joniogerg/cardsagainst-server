"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid = require("uuid/v4");
const fs = require("fs");
const winston = require("winston");
const { combine, timestamp, label, printf } = winston.format;
// import { Socket } from "socket.io";
const util_1 = require("./util");
const Subject_1 = require("rxjs/Subject");
exports.webDebug = null;
exports.gLogger = null;
exports.format = null;
// Listeners
exports.gameAdded = new Subject_1.Subject();
exports.gameRemoved = new Subject_1.Subject();
exports.gameChanged = new Subject_1.Subject();
const games = new util_1.ArrayList();
var loadedSets = new Array();
const emptySet = [];
var setsLoaded = false;
function createNewRoom(id, properties, host, webDebug) {
    return new Promise((resolve, reject) => {
        if (!setsLoaded) {
            resolve(null);
            return;
        }
        var gameRoom = new GameRoom(properties, host, webDebug);
        if (id != null && id != undefined) {
            gameRoom.ID = id;
        }
        gameRoom.logger = winston.createLogger({
            format: combine(label({ label: gameRoom.ID }), timestamp(), exports.format),
            transports: [
                new winston.transports.Console({
                    handleExceptions: true
                }),
                new winston.transports.File({
                    filename: "./logs/" + gameRoom.ID + ".log",
                    handleExceptions: true
                }),
                webDebug
            ]
        });
        games.add(gameRoom);
        exports.gameAdded.next(gameRoom);
        resolve(gameRoom);
    });
}
exports.createNewRoom = createNewRoom;
function getRoomByID(gameID) {
    for (var i = 0; i < games.length; i++) {
        if (games.get(i).ID == gameID) {
            return games.get(i);
        }
    }
    return null;
}
exports.getRoomByID = getRoomByID;
function getSet(name) {
    for (var i = 0; i < loadedSets.length; i++) {
        if (name == loadedSets[i].name)
            return loadedSets[i];
    }
    return null;
}
exports.getSet = getSet;
function getLegacySets() {
    var sets = new Array();
    for (let set of loadedSets) {
        if (set == null)
            continue;
        sets.push(set.toLegacy());
    }
    return sets;
}
exports.getLegacySets = getLegacySets;
function loadSets() {
    exports.gLogger.info("Loading sets..." + __dirname + "/database/sets/_prepared/");
    fs.readdir(__dirname + "/database/sets/_prepared/", (err, files) => {
        for (let file of files) {
            addSet(file);
        }
        setsLoaded = true;
        // loadedSets.sort((a, b) => {
        //     return a.name.localeCompare(b.name);
        // }); 
        // var tmp = new Array<Set>();
        // for(var i = 0; i < loadedSets.length; i++){
        //     if(loadedSets[i].name.startsWith("[")){
        //         tmp.push(loadedSets[i]);
        //         loadedSets.shift();
        //         i--;
        //     }
        // }
        // loadedSets = loadedSets.concat(tmp);
        exports.gLogger.info("Sets loaded successfuly!");
    });
}
exports.loadSets = loadSets;
function addSet(filename) {
    try {
        var file = fs.readFileSync("./database/sets/_prepared/" + filename, "utf8");
        var rawSet = JSON.parse(file);
        var set = new Set(rawSet.blackCards, rawSet.whiteCards, rawSet.name, rawSet.description, rawSet.official, rawSet.category);
        loadedSets.push(set);
        emptySet.push(new Set(null, null, set.name, set.description, set.official, set.category));
    }
    catch (err) {
        exports.gLogger.error(filename + " failed to load:");
        exports.gLogger.error(err);
        return;
    }
}
exports.addSet = addSet;
function getLegacyGames() {
    var legacyGames = new util_1.ArrayList();
    for (let game of games) {
        legacyGames.add(game.toLegacyGame());
    }
    return legacyGames;
}
exports.getLegacyGames = getLegacyGames;
class GameRoom {
    constructor(properties, _host, webDebug) {
        this.properties = properties;
        this._host = _host;
        this.webDebug = webDebug;
        this.ID = uuid().substr(0, 5);
        this.users = new util_1.ArrayList();
        this.czar = null;
        this.czarIndex = 0;
        // Nickname -> Cards
        this.choice = new util_1.HashMap();
        this.whiteCards = new util_1.ArrayList();
        this.blackCards = new util_1.ArrayList();
        this.blackCard = null;
        this.messages = [];
        this.host = null;
        this.logger = null;
        this.disconnecting = new util_1.ArrayList();
        this.destroied = false;
        this.gameState = GameState.LOBBY;
        this.curRound = 0;
        if (this.properties == undefined || this.properties == null) {
            this.properties = new Properties();
        }
        this.properties.cardSet.forEach(setName => {
            var set = getSet(setName);
            if (set != null) {
                this.whiteCards.addAll(set.whiteCards);
                this.blackCards.addAll(set.blackCards);
            }
        });
        for (var i = 0; i < this.properties.blankCards; i++) {
            this.whiteCards.add("#--!blank!--#");
        }
    }
    join(con) {
        if (this.destroied) {
            return;
        }
        this.logger.info(con.token);
        if (this.containsConnection(con)) {
            this.logger.error("Duplicated Connections!!");
            return;
        }
        if (this.gameState != GameState.LOBBY) {
            con.emit("err", { code: 3, message: "Game is already running!", data: this.ID });
            return;
        }
        if (this.users.length < this.properties.maxPlayers) {
            var isHost = false;
            if (con.token == this._host.token)
                isHost = true;
            if (con.nickname == undefined || con.nickname == null)
                return;
            var user = new ServerUser(con, isHost);
            this.logger.info("User " + user.name + " joined the game -> " + isHost);
            this.users.add(user);
            this.listenCardChoice(user);
            this.listenCzarChoice(user);
            this.sendUsers("users", this.getLegacyUsers());
            this.sendUsers("properties", this.properties);
            user.con.emit("self", user.toLegacy());
            user.con.emit("properties", this.properties);
            // user.socket.on("disconnecting", () => {
            //     this.leave(user.socket);
            // })
            // user.socket.on("disconnect", () => {
            //     if(this.gameState == GameState.LOBBY){
            //         this.leave(user.socket);
            //     }
            // })
            con.currentGame = this.ID;
            con.updateWebDebugStatus();
            this.webDebug.notifySockets("game users", { id: this.ID, users: this.getLegacyUsers() });
            user.con.on("home", () => {
                this.leave(user.con);
            });
            user.con.on("chat", msg => {
                this.addMessage(msg);
            });
            if (user.isHost) {
                user.con.on("start", () => {
                    this.start();
                });
            }
            exports.gameChanged.next(this);
        }
    }
    leave(con) {
        this.logger.info(con.token);
        var user = this.getUser(con.token);
        if (user != null) {
            this.logger.info("User " + user.name + " left the game");
            this.users.remove(user);
            this.choice.remove(user.name);
            if (this.users.length <= 1 && this.gameState != GameState.LOBBY) {
                this.destroy();
                return;
            }
            if (this.users.length <= 0) {
                this.destroy();
                return;
            }
            con.currentGame = null;
            con.updateWebDebugStatus();
            this.webDebug.notifySockets("game users", this.getLegacyUsers());
            if (this.destroied) {
                return;
            }
            if (user.isHost) {
                this.users.get(0).isHost = true;
            }
            if (this.gameState != GameState.LOBBY && this.czar != null && this.czar.con.token == con.token) {
                this.skip();
            }
            this.users.array.forEach(user => {
                this.updateUser(user);
            });
            exports.gameChanged.next(this);
        }
    }
    start() {
        if (this.gameState != GameState.LOBBY)
            return;
        this.checkBeforeStart();
        this.logger.info("Starting game");
        this.configureUsers();
        this.nextRound();
        for (var i = 0; i < this.users.length; i++) {
            this.whiteCards.add(this.users.get(i).name);
        }
        this.gameState = GameState.SELECTING_WHITE;
        this.sendUsers("started");
        exports.gameRemoved.next(this);
    }
    nextRound() {
        if (this.destroied)
            return;
        this.curRound += 1;
        if (this.curRound > 1) {
            for (var i = 0; i < this.users.length; i++) {
                this.logger.info(this.users.get(i).cards.length);
                while (this.users.get(i).cards.length != this.properties.whiteCardAmount) {
                    var index = Math.floor(Math.random() * this.whiteCards.length);
                    var card = this.whiteCards.get(index);
                    if (card == null) {
                        this.finish();
                        return;
                    }
                    this.whiteCards.removeAt(index);
                    this.users.get(i).giveCard(card);
                }
                if (this.users.get(i).score == this.properties.pointsToWin) {
                    this.finish(this.users.get(i));
                    return;
                }
            }
        }
        this.sendUsers("round");
        this.gameState = GameState.SELECTING_WHITE;
        this.choice.clear();
        this.sendCards();
        this.sendUsers("users", this.getLegacyUsers());
        this.sendUsers("remaining", this.users.length - 1 - this.choice.size());
        var blackCardIndex = Math.floor(Math.random() * this.blackCards.length);
        var blackCard = this.blackCards.get(blackCardIndex);
        if (blackCard == null) {
            this.finish();
            return;
        }
        this.blackCard = blackCard;
        this.blackCards.removeAt(blackCardIndex);
        this.sendUsers("cards black", blackCard);
        this.logger.info("BlackCard: " + blackCard.content);
        this.updateCzar();
        this.sendUsers("winnerRound", null);
        this.sendUsers("gamestate", this.gameState);
    }
    finish(user = null) {
        if (this.destroied) {
            return;
        }
        if (user == null) {
            for (let u of this.users) {
                if (u.score == this.properties.pointsToWin) {
                    user = u;
                    break;
                }
            }
        }
        this.sendUsers("finish", user.toLegacy());
        return;
    }
    skip() {
        if (this.destroied) {
            return;
        }
        this.nextRound();
        this.sendUsers("skip");
    }
    sendCards() {
        if (this.destroied) {
            return;
        }
        this.users.array.forEach(user => {
            user.con.emit("cards white", user.cards);
            user.con.emit("self", user.toLegacy());
        });
        this.sendUsers("choices", this.getLegacyChoices());
    }
    listenCardChoice(user) {
        user.con.on("choose card", card => {
            if (this.destroied)
                return;
            if (this.gameState != GameState.SELECTING_WHITE)
                return;
            if (user.con.token == this.czar.con.token) {
                return;
            }
            this.logger.info(JSON.stringify(card.array.length + "; " + this.blackCard.pick));
            if (card == null || card == undefined || card.array == undefined || card.array.length != this.blackCard.pick) {
                return;
            }
            this.choice.push(user.token, new util_1.ArrayList(card.array));
            this.logger.info("Choices 1: " + this.choice.size());
            this.sendUsers("remaining", this.users.length - 1 - this.choice.size());
            if (this.choice.size() == this.users.length - 1) {
                this.gameState = GameState.SELECTING_CZAR;
                this.sendUsers("gamestate", this.gameState);
                this.logger.info("Choices 2: " + this.choice.size());
                this.randomizeSelections();
                this.users.array.forEach(user => {
                    if (user.token == this.czar.token)
                        return;
                    // this.logger.info(JSON.stringify(this.getLegacyChoices()))
                    this.logger.info(JSON.stringify(this.choice));
                    this.logger.info("Choices 3: " + this.choice.size());
                    this.logger.info(user.token);
                    user.cards.removeAll(this.choice.get(user.token).array);
                });
                this.sendCards();
                this.sendUsers("czar select");
            }
        });
    }
    listenCzarChoice(user) {
        // SEND THIS AS STRING ARRAY!!!
        user.con.on("choose czar", card => {
            if (this.destroied)
                return;
            if (this.gameState != GameState.SELECTING_CZAR) {
                return;
            }
            if (user.con.token != this.czar.con.token)
                return;
            this.gameState = GameState.WAITING;
            this.sendUsers("gamestate", this.gameState);
            this.logger.info(card);
            var winner;
            this.logger.info("Looking for winner...");
            for (let choice of this.choice.map) {
                this.logger.info(choice, choice.value);
                var right = false;
                for (var i = 0; i < card.length; i++) {
                    this.logger.info(choice.value.array[i] + "; " + card[i]);
                    right = choice.value.array.indexOf(card[i]) != -1;
                }
                if (right) {
                    winner = choice;
                    break;
                }
            }
            this.logger.info(winner.key);
            this.getUser(winner.key).wonRound();
            this.sendUsers("winner round", new util_1.Pair(this.getUser(winner.key).toLegacy(), winner.value));
            this.sendUsers("users", this.getLegacyUsers());
            setTimeout(() => {
                this.nextRound();
            }, 5000);
        });
    }
    unsubscribeListener(user, event) {
        user.socket.removeAllListeners(event);
    }
    sendUsers(event, data = null) {
        this.users.array.forEach(user => {
            user.con.emit(event, data);
        });
    }
    updateCzar() {
        this.czarIndex++;
        if (this.czarIndex >= this.users.length) {
            this.czarIndex = 0;
        }
        this.czar = this.users.get(this.czarIndex);
        this.sendUsers("czar", this.czar.toLegacy());
    }
    checkBeforeStart() {
    }
    updateUser(user) {
        if (user == null && user == undefined && user.con == undefined)
            return;
        user.con.emit("users", this.getLegacyUsers());
        user.con.emit("properties", this.properties);
        user.con.emit("gamestate", this.gameState);
        user.con.emit("started");
        user.con.emit("remaining", this.users.length - 1 - this.choice.size());
        user.con.emit("cards black", this.blackCard);
        user.con.emit("cards white", user.cards);
        user.con.emit("self", user.toLegacy());
        user.con.emit("choices", this.getLegacyChoices());
        if (this.czar != null)
            user.con.emit("czar", this.czar.toLegacy());
        user.con.emit("msgs", this.messages);
        this.listenCardChoice(user);
        this.listenCzarChoice(user);
    }
    configureUsers() {
        for (var u = 0; u < this.users.length; u++) {
            var user = this.users.get(u);
            for (var i = 1; i <= this.properties.whiteCardAmount; i++) {
                var index = Math.floor(Math.random() * this.whiteCards.length);
                var card = this.whiteCards.get(index);
                this.whiteCards.removeAt(index);
                user.giveCard(card);
            }
        }
        this.sendCards();
    }
    updatePlayerInfo(con) {
        if (this.gameState == GameState.LOBBY)
            return;
        var user = this.getUser(con.token);
        this.logger.info("User tries to reconnect...");
        if (user) {
            this.updateUser(user);
        }
    }
    /*
        CHAT
    */
    addMessage(msg) {
        this.messages.push(msg);
        this.sendUsers("chat", msg);
    }
    getLegacyChoices() {
        var map = new util_1.HashMap();
        for (var i = 0; i < this.choice.size(); i++) {
            var sUser = this.choice.map.get(i).key;
            map.push(this.getUser(sUser).toLegacy(), this.choice.map.get(i).value);
        }
        return map;
    }
    getLegacyUsers() {
        var map = [];
        for (var i = 0; i < this.users.length; i++) {
            var sUser = this.users.get(i);
            if (!sUser.toLegacy)
                continue;
            map.push(sUser.toLegacy());
        }
        return map;
    }
    getTestUser() {
        var map = [];
        for (var i = 0; i < this.users.length; i++) {
            var sUser = this.users.get(i);
            map.push(sUser.con.token);
        }
        return map;
    }
    getUser(id) {
        for (var i = 0; i < this.users.length; i++) {
            var user = this.users.get(i);
            if (user.con.token == id) {
                return user;
            }
        }
    }
    containsConnection(con) {
        for (var i = 0; i < this.users.length; i++) {
            if (this.users.get(i).con.token == con.token)
                return true;
        }
        return false;
    }
    randomizeSelections() {
        var old = this.choice.clone();
        var tmp = new util_1.HashMap();
        for (var i = 0; i < this.choice.size(); i++) {
            var random = Math.max(0, Math.floor(Math.random() * old.size()) - 1);
            this.logger.info("R: " + random);
            var pair = old.getByIndex(random);
            this.logger.info("R: " + JSON.stringify(pair));
            tmp.push(pair.key, pair.value);
            this.logger.info("R: " + tmp.size());
            old.remove(pair.key);
            this.logger.info("R: " + old.size());
        }
        this.choice = tmp;
    }
    destroy() {
        this.logger.info("No players in gameroom\"" + this.ID + "\". Destroying game...");
        this.destroied = true;
        this.sendUsers("closed");
        games.remove(this);
        exports.gameRemoved.next(this);
    }
    toLegacyGame() {
        return {
            ID: this.ID,
            users: this.getLegacyUsers(),
            gameState: this.gameState,
            properties: this.properties
        };
    }
}
exports.GameRoom = GameRoom;
var GameState;
(function (GameState) {
    GameState[GameState["SELECTING_WHITE"] = 0] = "SELECTING_WHITE";
    GameState[GameState["SELECTING_CZAR"] = 1] = "SELECTING_CZAR";
    GameState[GameState["WAITING"] = 2] = "WAITING";
    GameState[GameState["LOBBY"] = 3] = "LOBBY";
})(GameState = exports.GameState || (exports.GameState = {}));
class Properties {
    constructor(maxPlayers = 8, pointsToWin = 8, cardSet = [
        "Base Set"
    ], timeToSet = -1, whiteCardAmount = 6, blankCards = 0) {
        this.maxPlayers = maxPlayers;
        this.pointsToWin = pointsToWin;
        this.cardSet = cardSet;
        this.timeToSet = timeToSet;
        this.whiteCardAmount = whiteCardAmount;
        this.blankCards = blankCards;
    }
}
exports.Properties = Properties;
class LegacyUser {
    constructor(name, token, isHost, cards = new util_1.ArrayList(), score = 0) {
        this.score = score;
        this.name = name;
        this.token = token;
        this.isHost = isHost;
        this.cards = cards;
    }
}
exports.LegacyUser = LegacyUser;
class ServerUser {
    constructor(con, isHost, cards = new util_1.ArrayList(), score = 0) {
        this.con = con;
        this.score = score;
        this.isHost = isHost;
        this.cards = cards;
    }
    giveCard(card) {
        this.cards.add(card);
    }
    wonRound() {
        this.score += 1;
    }
    toLegacy() {
        return new LegacyUser(this.name, this.con.token, this.isHost, this.cards, this.score);
    }
    get name() {
        return this.con.nickname;
    }
    get token() {
        return this.con.token;
    }
}
exports.ServerUser = ServerUser;
class BlackCard {
    constructor(content, pick = 1) {
        this.content = content;
        this.pick = pick;
    }
}
exports.BlackCard = BlackCard;
class Set {
    constructor(blackCards = [], whiteCards = [], name = "", description = "No description", official = false, category = "") {
        this.blackCards = blackCards;
        this.whiteCards = whiteCards;
        this.name = name;
        this.description = description;
        this.official = official;
        this.category = category;
    }
    toLegacy() {
        return new LegacySet(this.name, this.description, this.official, this.whiteCards.length, this.blackCards.length, this.category);
    }
}
exports.Set = Set;
class LegacySet {
    constructor(name = "", description = "No description", official = false, whiteCards = 0, blackCards = 0, category = "") {
        this.name = name;
        this.description = description;
        this.official = official;
        this.whiteCards = whiteCards;
        this.blackCards = blackCards;
        this.category = category;
    }
}
exports.LegacySet = LegacySet;
class Message {
    constructor(user, text) {
        this.user = user;
        this.text = text;
    }
}
exports.Message = Message;
//# sourceMappingURL=game.js.map