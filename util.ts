export class Pair<K, V>{
    constructor(
        public key: K,
        public value: V
    ){}
}

export class HashMap<K, V> implements Map<K, V>{
    
    map: ArrayList<Pair<K, V>> = new ArrayList();

    push(key: K, value: V){
        var i = this.containsKey(key);
        if(i >= 0){
            this.map.get(i).value = value;
        }else{
            this.map.add(new Pair(key, value));
        }
    }

    remove(key: K){
        this.map.removeAt(this.indexAt(key));
        
    }
 
    removeValue(value: V){
        for(var i = 0; i < this.map.length; i++){
            if(value == this.map.get(i).value){
                this.map.removeAt(i);
                return;
            }
        }
    }

    removeAt(index: number){
        this.map.removeAt(index);
        
    }

    get(key: K): V{
        for(var i = 0; i < this.map.length; i++){
            if(this.map.get(i).key == key){
                return this.map.get(i).value;
            }
        }
    }

    indexAt(key: K): number{
        for(var i = 0; i < this.map.length; i++){
            if(this.map.get(i).key == key){
                return i;
            }
        }
    }

    getByIndex(index: number): Pair<K, V>{
        return this.map.get(index);
    }

    getByValue(value: V): Pair<K,V>{
        for(var i = 0; i < this.map.length; i++){
                if(this.map.get(i).value == value){
                    return this.map.get(i);
                }
            
        }
        return null;
    }

    containsKey(key: K): number{
        for(var i = 0; i < this.map.length; i++){
            if(this.map.get(i) != null && this.map.get(i).key == key){
                return i;
            }
        }
        return -1;
    }

    size(): number{
        return this.map.length;
    }



    clear(){
        this.map = new ArrayList();
    }


    copyInto(map: Map<K, V>){
        this.map = new ArrayList<Pair<K, V>>();
        this.map.copyInto(map.map);
        
    }

    clone(): HashMap<K, V>{
        var clone = new HashMap<K, V>();
        for(let pair of this.map){
            clone.push(pair.key, pair.value);
        }
        return clone;
    }
}


export class MulitHashMap<K, V> extends HashMap<K, ArrayList<V>>{

    push(key, value){
        var i = this.containsKey(key);
        if(i >= 0){
            this.map.get(i).value.add(value);
        }else{
            this.map.add(new Pair(key, new ArrayList([value])));
        }

    }

    removeItem(key: K, value: V){
        var i = this.containsKey(key);
        if(i >= 0){
            this.map.get(i).value.remove(value);
            if(this.map.get(i).value.length == 0){
                this.map.removeAt(i);
            }
        }
    }

    removeItemAt(key: K, index: number){
        var i = this.containsKey(key);
        if(i >= 0){
            try{
                var array = this.map.get(i).value;
                array.removeAt(index);
                this.map.set(i, new Pair(key, array))
            }catch(e){
                throw e;
            }
        }
    }
    
}

export class ArrayList<T> implements Iterable<T>, List<T>{
    constructor(public array: Array<T> = []){}


    add(value: T){
        this.array.push(value);
    }

    remove(value: T): boolean{
        var arraySize = this.array.length + 0;
        var tmp = new Array<T>();
        for(var i = 0; i < this.array.length; i++){
            if(this.array[i] != value)
                tmp.push(this.array[i]);
        }
        this.array = tmp;
        return arraySize > tmp.length;
    }

    removeAll(value: T[]){
        var tmp = new Array<T>();
        var v = new ArrayList<T>(value);
        for(var i = 0; i < this.array.length; i++){
            if(!v.contains(this.array[i])){
                tmp.push(this.array[i]);
            }
        }
        console.log(tmp);
        
        this.array = tmp;
    }

    removeAt(index){
        var tmp = new Array<T>();
        for(var i = 0; i < this.array.length; i++){
            if(i != index)
                tmp.push(this.array[i]);
        }
        this.array = tmp;
    }

    get(index: number){
        return this.array[index];
    }
    
    contains(value: T){
        for(var i = 0; i < this.array.length; i++){
            if(value == this.array[i]){
                return true;
            }
        }
        return false;
    }

    addAll(array: T[]){
        this.array = this.array.concat(array);
    }

    getIndexAt(value: T){
        for(var i = 0; i < this.array.length; i++){
            if(this.array[i] == value)
                return i;
        }
    }

    get length(): number {
        return this.array.length;
    }

    clear(){
        this.array = new Array<T>();
    } 

    copyInto(list: List<T>){
        this.array = list.array;
    }
    
    set(index: number, value: T) {
        this.array[index] = value; 
    }

    [Symbol.iterator]() {
        let pointer = 0;
        let array = this.array;
    
        return {
            next(): IteratorResult<T> {
                if (pointer < array.length) {
                    return {
                        done: false,
                        value: array[pointer++]
                    }
                } else {
                    return {
                        done: true,
                        value: null
                    }
                }
            }
        }
    }
}



export interface List<T>{
    length:number;
    array: T[];
    add(value: T);
    remove(value: T);
    removeAll(value: T[]);
    removeAt(index);
    get(index: number);
    contains(value: T);
    addAll(array: T[]);
    getIndexAt(value: T);
    clear();
    set(index: number, value: T)
}

export interface Map<K, V>{
    map: ArrayList<Pair<K, V>>;
    push(key, value)
    remove(key)
    get(key: K): V
    getByIndex(index: number): Pair<K, V>
    getByValue(value: V): Pair<K,V>
    containsKey(key): number
    size(): number
    clear()
}
export interface Parsable<T>{
    fromJson(obj): T
}