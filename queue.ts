

export class Queue<V, R>{

    private queue: Array<PendingResult<V, R>> = [];

    public tasksRunning: number = 0;


    constructor(private run:(value: V, resolve: (result: R) => void, reject: (initValue: V) => void) => void, private maxTasks = 1, public retryOnFailure: boolean = true){

    }


    public next(){
        if(this.maxTasks <= this.tasksRunning || this.queue.length == 0){
            return;
        }

        var pRes = this.queue[0];

        pRes.ready = true;

        this.tasksRunning++;
        pRes.run()

        this.queue = this.queue.slice(1, this.queue.length);
    }


    public add(value: V): PendingResult<V, R>{
        var res = new PendingResult<V, R>(this, value, this.run);

        this.queue.push(res);

        this.next();

        return res;
    }


    public continueQueue(){

    }

    public pauseQueue(){

    }

}

export class PendingResult<V, R>{

    private resultFunc: (R) => void;
    private canceledFunc: (initValue: V) => void;
    private _resultFunc: (R) => void;
    private _canceledFunc: (initValue: V) => void = (v) => {console.log("Test " + v)};
    private result: R = null;

    public rejected = true;
    public ready: boolean = false;
    public executed: boolean = false;

    constructor(private queue: Queue<V, R>, private value: V, public runFunc:(value: V, resolve: (result: R) => void, reject: (initValue: V) => void) => void){
        
    }


    next(result: R){
        if(this.resultFunc != null){
            this.resultFunc(result)
        }
    }

    public then(func:(R) => void): PendingResult<V, R>{
        this._resultFunc = func;
        this.resultFunc = (r) => {
            func(r);
            this.queue.tasksRunning--,
            this.queue.next()
        };
        return this;
    } 

    public canceled(func: (initValue: V) => void): PendingResult<V, R>{
        this._canceledFunc = func;
        this.canceledFunc = (v) => {
            console.log("Canceled");
            this.rejected = true;
            func(v);
            if(this.queue.retryOnFailure){
                var clone = this.cleanClone();
                clone.ready = true;
                clone.run();
            }else{
                this.queue.tasksRunning--,
                this.queue.next();
            }
        };

        
        return this;
    }

    public run(){

        if(this.resultFunc == null || !this.ready || this.executed){
            return;
        }
        this.runFunc(this.value, (v) => {
            this.result = v;
            this.resultFunc(v);
            
        }, this.canceledFunc);

    }

    public cleanClone(): PendingResult<V, R>{
        var pRes = new PendingResult(this.queue, this.value, this.runFunc);
        pRes.then(this._resultFunc).canceled(this._canceledFunc);
        return pRes;
    }

}