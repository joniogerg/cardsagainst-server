"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Pair {
    constructor(key, value) {
        this.key = key;
        this.value = value;
    }
}
exports.Pair = Pair;
class HashMap {
    constructor() {
        this.map = new ArrayList();
    }
    push(key, value) {
        var i = this.containsKey(key);
        if (i >= 0) {
            this.map.get(i).value = value;
        }
        else {
            this.map.add(new Pair(key, value));
        }
    }
    remove(key) {
        this.map.removeAt(this.indexAt(key));
    }
    removeValue(value) {
        for (var i = 0; i < this.map.length; i++) {
            if (value == this.map.get(i).value) {
                this.map.removeAt(i);
                return;
            }
        }
    }
    removeAt(index) {
        this.map.removeAt(index);
    }
    get(key) {
        for (var i = 0; i < this.map.length; i++) {
            if (this.map.get(i).key == key) {
                return this.map.get(i).value;
            }
        }
    }
    indexAt(key) {
        for (var i = 0; i < this.map.length; i++) {
            if (this.map.get(i).key == key) {
                return i;
            }
        }
    }
    getByIndex(index) {
        return this.map.get(index);
    }
    getByValue(value) {
        for (var i = 0; i < this.map.length; i++) {
            if (this.map.get(i).value == value) {
                return this.map.get(i);
            }
        }
        return null;
    }
    containsKey(key) {
        for (var i = 0; i < this.map.length; i++) {
            if (this.map.get(i) != null && this.map.get(i).key == key) {
                return i;
            }
        }
        return -1;
    }
    size() {
        return this.map.length;
    }
    clear() {
        this.map = new ArrayList();
    }
    copyInto(map) {
        this.map = new ArrayList();
        this.map.copyInto(map.map);
    }
    clone() {
        var clone = new HashMap();
        for (let pair of this.map) {
            clone.push(pair.key, pair.value);
        }
        return clone;
    }
}
exports.HashMap = HashMap;
class MulitHashMap extends HashMap {
    push(key, value) {
        var i = this.containsKey(key);
        if (i >= 0) {
            this.map.get(i).value.add(value);
        }
        else {
            this.map.add(new Pair(key, new ArrayList([value])));
        }
    }
    removeItem(key, value) {
        var i = this.containsKey(key);
        if (i >= 0) {
            this.map.get(i).value.remove(value);
            if (this.map.get(i).value.length == 0) {
                this.map.removeAt(i);
            }
        }
    }
    removeItemAt(key, index) {
        var i = this.containsKey(key);
        if (i >= 0) {
            try {
                var array = this.map.get(i).value;
                array.removeAt(index);
                this.map.set(i, new Pair(key, array));
            }
            catch (e) {
                throw e;
            }
        }
    }
}
exports.MulitHashMap = MulitHashMap;
class ArrayList {
    constructor(array = []) {
        this.array = array;
    }
    add(value) {
        this.array.push(value);
    }
    remove(value) {
        var arraySize = this.array.length + 0;
        var tmp = new Array();
        for (var i = 0; i < this.array.length; i++) {
            if (this.array[i] != value)
                tmp.push(this.array[i]);
        }
        this.array = tmp;
        return arraySize > tmp.length;
    }
    removeAll(value) {
        var tmp = new Array();
        var v = new ArrayList(value);
        for (var i = 0; i < this.array.length; i++) {
            if (!v.contains(this.array[i])) {
                tmp.push(this.array[i]);
            }
        }
        console.log(tmp);
        this.array = tmp;
    }
    removeAt(index) {
        var tmp = new Array();
        for (var i = 0; i < this.array.length; i++) {
            if (i != index)
                tmp.push(this.array[i]);
        }
        this.array = tmp;
    }
    get(index) {
        return this.array[index];
    }
    contains(value) {
        for (var i = 0; i < this.array.length; i++) {
            if (value == this.array[i]) {
                return true;
            }
        }
        return false;
    }
    addAll(array) {
        this.array = this.array.concat(array);
    }
    getIndexAt(value) {
        for (var i = 0; i < this.array.length; i++) {
            if (this.array[i] == value)
                return i;
        }
    }
    get length() {
        return this.array.length;
    }
    clear() {
        this.array = new Array();
    }
    copyInto(list) {
        this.array = list.array;
    }
    set(index, value) {
        this.array[index] = value;
    }
    [Symbol.iterator]() {
        let pointer = 0;
        let array = this.array;
        return {
            next() {
                if (pointer < array.length) {
                    return {
                        done: false,
                        value: array[pointer++]
                    };
                }
                else {
                    return {
                        done: true,
                        value: null
                    };
                }
            }
        };
    }
}
exports.ArrayList = ArrayList;
//# sourceMappingURL=util.js.map