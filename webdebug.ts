
import * as Transport from "winston-transport"
import { Socket } from "socket.io";
import { ArrayList } from "./util";

export class WebDebug extends Transport{

    sockets = new ArrayList<Socket>();
    
    constructor(opts) {
        super(opts);
    }

    log(info, callback = null) {
        setImmediate(() => {
            this.emit('logged', info);
            this.notifySockets("log", info);
        });

        if(callback)
            callback();
    }

    registerSocket(socket: Socket){
        console.log("Registering socket...");
        if(!this.sockets.contains(socket)){
            this.sockets.add(socket);
            socket.on("debug unregister", () => {
                // Sends feedback if socket could not be removed
                if(!this.sockets.remove(socket)){
                    console.info("Sockets could not be removed from a WebDebugger");
                }
            });
            console.log("Socket registered for WebDebug");
        }
    }

    notifySockets(tag: string, data: any){
        this.sockets.array.forEach(socket => {

            socket.emit("debug " + tag, data);
        });
    }

    notifySocketsDiagnostic(tag: string, data: Diagnosticable){
        this.notifySockets(tag, data.diagnostic())
    }

}


export interface Diagnosticable{
    diagnostic(): Object;
}