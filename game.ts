
import * as uuid from "uuid/v4";
import * as fs from "fs";
import * as winston from "winston";
const { combine, timestamp, label, printf } = winston.format;
// import { Socket } from "socket.io";
import { Map, List, Pair, HashMap, ArrayList } from "./util";
import { Connection } from "./connectionHandler";
import { Subject } from "rxjs/Subject";
import { Server } from "http";
import { WebDebug } from "./webdebug";


export var webDebug = null;
export var gLogger = null;
export var format = null;
 
// Listeners
export const gameAdded: Subject<GameRoom> = new Subject<GameRoom>();
export const gameRemoved: Subject<GameRoom> = new Subject<GameRoom>();
export const gameChanged: Subject<GameRoom> = new Subject<GameRoom>();


const games:ArrayList<GameRoom> = new ArrayList();
var loadedSets: Array<Set> = new Array<Set>();
const emptySet: Array<Set> = [];
var setsLoaded: boolean = false;

export function createNewRoom(id:string, properties: Properties, host: Connection, webDebug: WebDebug): Promise<GameRoom>{
    return new Promise<GameRoom>((resolve, reject) => {
        if(!setsLoaded){
            resolve(null);
            return;
        }

        
        var gameRoom = new GameRoom(properties, host, webDebug);
        if(id != null && id != undefined){
            gameRoom.ID = id;
        }



        gameRoom.logger = winston.createLogger({
            format: combine(
                label({ label: gameRoom.ID }),
                timestamp(),
                format
            ),
            transports: [
                new winston.transports.Console({
                    handleExceptions: true
                }),
                new winston.transports.File({
                    filename: "./logs/" + gameRoom.ID + ".log",
                    handleExceptions: true
                }),
                webDebug
            ]
        })
        games.add(gameRoom);

        gameAdded.next(gameRoom);

        resolve(gameRoom);
    });
}

export function getRoomByID(gameID){
    for(var i = 0; i < games.length; i++){
        if(games.get(i).ID == gameID){
            return games.get(i);
        }
    } 
    return null;   
}

export function getSet(name): Set{
    for(var i = 0; i < loadedSets.length; i++){
        if(name == loadedSets[i].name)
            return loadedSets[i];
    }
    return null;
}

export function getLegacySets(): Array<LegacySet>{
    var sets = new Array<LegacySet>();
    for(let set of loadedSets){
        if(set == null)
        continue;
        sets.push(set.toLegacy());
    }
    return sets;
}

export function loadSets(){
    gLogger.info("Loading sets..." + __dirname + "/database/sets/_prepared/");
    fs.readdir( __dirname + "/database/sets/_prepared/", (err, files) => {
        for(let file of files){ 
            addSet(file);
        }
        setsLoaded = true;
        // loadedSets.sort((a, b) => {
        //     return a.name.localeCompare(b.name);
        // }); 

        // var tmp = new Array<Set>();
        // for(var i = 0; i < loadedSets.length; i++){
        //     if(loadedSets[i].name.startsWith("[")){
        //         tmp.push(loadedSets[i]);
        //         loadedSets.shift();
        //         i--;
        //     }
        // }
        // loadedSets = loadedSets.concat(tmp);
        gLogger.info("Sets loaded successfuly!");
    });
}

export function addSet(filename: string){
    try{
        var file = fs.readFileSync("./database/sets/_prepared/" + filename, "utf8");
        var rawSet = JSON.parse(file);
        var set = new Set(rawSet.blackCards, rawSet.whiteCards, rawSet.name, rawSet.description, rawSet.official, rawSet.category);
        loadedSets.push(set);
        emptySet.push(new Set(null, null, set.name, set.description, set.official, set.category));
    }catch(err){
        gLogger.error(filename + " failed to load:")
        gLogger.error(err);
        return;
    }
}

export function getLegacyGames(): ArrayList<any>{

    var legacyGames = new ArrayList<any>();

    for(let game of games){
        legacyGames.add(game.toLegacyGame());
    }

    return legacyGames;
}

export class GameRoom{

    ID: string = uuid().substr(0, 5);
    users: ArrayList<ServerUser> = new ArrayList(); 
    czar: ServerUser = null;
    czarIndex: number = 0;
    // Nickname -> Cards
    choice: HashMap<string, List<string>> = new HashMap();
    whiteCards: List<string> = new ArrayList();
    blackCards: List<BlackCard> = new ArrayList();
    blackCard: BlackCard = null;
    messages: Array<Message> = [];
    host: ServerUser = null;
    
    logger: any = null;

    disconnecting: ArrayList<User> = new ArrayList<User>();
    destroied: boolean = false;

    public gameState: GameState = GameState.LOBBY;

    curRound = 0;

    constructor(public properties: Properties, public _host: Connection, public webDebug: WebDebug){
        if(this.properties == undefined || this.properties == null){
            this.properties = new Properties();
        }
        this.properties.cardSet.forEach(setName => {
            var set = getSet(setName);
            if(set != null){
                this.whiteCards.addAll(set.whiteCards);
                this.blackCards.addAll(set.blackCards);
            }
            
        });

        for(var i = 0; i < this.properties.blankCards; i++){
            this.whiteCards.add("#--!blank!--#");
        }
    } 

    join(con: Connection){
        if(this.destroied){
            return;
        }
        this.logger.info(con.token);

        if(this.containsConnection(con) ){
            this.logger.error("Duplicated Connections!!");
            return;
        }
        if(this.gameState != GameState.LOBBY){
            con.emit("err", {code: 3, message: "Game is already running!", data: this.ID});
            return;
        }
        if(this.users.length < this.properties.maxPlayers){ 
            var isHost = false;
            if(con.token == this._host.token)
                isHost = true;
            if(con.nickname == undefined || con.nickname == null)
                return;
            var user = new ServerUser(con, isHost);
            this.logger.info("User " + user.name + " joined the game -> " + isHost);

            this.users.add(user);

            this.listenCardChoice(user);
            this.listenCzarChoice(user);

            this.sendUsers("users", this.getLegacyUsers());
            this.sendUsers("properties", this.properties);

            user.con.emit("self", user.toLegacy())  
            user.con.emit("properties", this.properties);

            // user.socket.on("disconnecting", () => {
            //     this.leave(user.socket);
            // })
            // user.socket.on("disconnect", () => {
            //     if(this.gameState == GameState.LOBBY){
            //         this.leave(user.socket);
            //     }
            // })

            con.currentGame = this.ID;
            con.updateWebDebugStatus();
            
            this.webDebug.notifySockets("game users", {id: this.ID, users: this.getLegacyUsers()})

            user.con.on("home", () => {
                this.leave(user.con);
            })
            
            user.con.on("chat", msg => {
                this.addMessage(<Message>msg);
            })

            if(user.isHost){
                user.con.on("start", () => {
                    this.start();
                })
            }
            gameChanged.next(this);


        }
    }

    public leave(con: Connection){
        this.logger.info(con.token);
        var user = this.getUser(con.token);
        if(user != null){
            this.logger.info("User " + user.name + " left the game");
            this.users.remove(user);
            this.choice.remove(user.name);
            if(this.users.length <= 1&& this.gameState != GameState.LOBBY){
                this.destroy();
                return;
            }
            if(this.users.length <= 0){
                this.destroy();
                return;
            }
            
            con.currentGame = null;
            con.updateWebDebugStatus();
            
            this.webDebug.notifySockets("game users", this.getLegacyUsers())

            if(this.destroied){
                return;
            }

            if(user.isHost){
                this.users.get(0).isHost = true;
            }
            if(this.gameState != GameState.LOBBY && this.czar != null && this.czar.con.token == con.token){
                this.skip();
            }
            this.users.array.forEach(user => {
                this.updateUser(user);
            })
            gameChanged.next(this);

            

            
        }
    }

    start(){
        if(this.gameState != GameState.LOBBY)
            return;

        this.checkBeforeStart();
        this.logger.info("Starting game");
        this.configureUsers();

        this.nextRound();

        for(var i = 0; i < this.users.length; i++){
            this.whiteCards.add(this.users.get(i).name);
        }

        this.gameState = GameState.SELECTING_WHITE;
        this.sendUsers("started");

        gameRemoved.next(this);
        
    }

    nextRound(){
        if(this.destroied)
            return;
        this.curRound += 1;
        if(this.curRound > 1){
            for(var i = 0; i < this.users.length; i++){
                this.logger.info(this.users.get(i).cards.length);
                while(this.users.get(i).cards.length != this.properties.whiteCardAmount){
                    var index = Math.floor(Math.random() * this.whiteCards.length);
                    var card = this.whiteCards.get(index);
                    if(card == null){
                        this.finish();
                        return;
                    }
                    this.whiteCards.removeAt(index);
                    this.users.get(i).giveCard(card);
                }

                if(this.users.get(i).score == this.properties.pointsToWin){
                    this.finish(this.users.get(i));
                    return;
                }
            }
        }
        this.sendUsers("round");

        this.gameState = GameState.SELECTING_WHITE;

        this.choice.clear();
        
        this.sendCards();
        this.sendUsers("users", this.getLegacyUsers());
        this.sendUsers("remaining", this.users.length - 1 - this.choice.size());

        var blackCardIndex = Math.floor(Math.random() * this.blackCards.length);
        var blackCard = this.blackCards.get(blackCardIndex);
        if(blackCard == null){
            this.finish();
            return;
        }
        this.blackCard = blackCard;
        this.blackCards.removeAt(blackCardIndex);
        this.sendUsers("cards black", blackCard);
        this.logger.info("BlackCard: " + blackCard.content);

        this.updateCzar();

        this.sendUsers("winnerRound", null);

        this.sendUsers("gamestate", this.gameState);

        
    }

    finish(user = null){
        if(this.destroied){
            return;
        }
        if(user == null){
            for(let u of this.users){
                if(u.score == this.properties.pointsToWin){
                    user = u;
                    break;
                }
            }
        }
        
        this.sendUsers("finish", user.toLegacy());
        return;
    }
    skip(){
        if(this.destroied){
            return;
        }
        this.nextRound();
        this.sendUsers("skip");
    }

    sendCards(){
        if(this.destroied){
            return;
        }
        this.users.array.forEach(user => {
            user.con.emit("cards white", user.cards);
            user.con.emit("self", user.toLegacy());
        })
        this.sendUsers("choices", this.getLegacyChoices());
    }

    listenCardChoice(user: ServerUser){
        user.con.on("choose card", card => {
            if(this.destroied)
                return;
            if(this.gameState != GameState.SELECTING_WHITE)
                return;
                
            if(user.con.token == this.czar.con.token){
                return;
            }

            this.logger.info(JSON.stringify(card.array.length + "; " + this.blackCard.pick));
            if(card == null || card == undefined || card.array == undefined || card.array.length != this.blackCard.pick){
                return;
            }
            
            
            this.choice.push(user.token, new ArrayList<string>(card.array));
            this.logger.info("Choices 1: " + this.choice.size());
            this.sendUsers("remaining", this.users.length - 1 - this.choice.size());

            if(this.choice.size() == this.users.length - 1){
                this.gameState = GameState.SELECTING_CZAR;
                this.sendUsers("gamestate", this.gameState);
                this.logger.info("Choices 2: " + this.choice.size());
                this.randomizeSelections();
                this.users.array.forEach(user => {
                    if(user.token == this.czar.token)
                        return;
                    // this.logger.info(JSON.stringify(this.getLegacyChoices()))

                    this.logger.info(JSON.stringify(this.choice));
                    this.logger.info("Choices 3: " + this.choice.size());
                    this.logger.info(user.token);
                    user.cards.removeAll(this.choice.get(user.token).array);
                });
                this.sendCards();
                this.sendUsers("czar select");
            }
        });
    }

    listenCzarChoice(user: ServerUser){
        // SEND THIS AS STRING ARRAY!!!
        user.con.on("choose czar", card => {
            if(this.destroied)
                return;
            if(this.gameState != GameState.SELECTING_CZAR){
                return;
            }
            if(user.con.token != this.czar.con.token)
                return;

            this.gameState = GameState.WAITING;
            this.sendUsers("gamestate", this.gameState);

            this.logger.info(card);
            var winner;
            this.logger.info("Looking for winner...");
            for(let choice of this.choice.map){
                this.logger.info(choice, choice.value);
                var right = false;
                for(var i = 0; i < card.length; i++){
                    this.logger.info(choice.value.array[i] + "; " + card[i]);
                    right = choice.value.array.indexOf(card[i]) != -1;
                }
                if(right){
                    winner = choice;
                    break;
                }
                    
            }

            this.logger.info(winner.key);

            this.getUser(winner.key).wonRound();
            this.sendUsers("winner round", new Pair<LegacyUser, List<string>>(this.getUser(winner.key).toLegacy(), winner.value));
            this.sendUsers("users", this.getLegacyUsers());
            setTimeout(() => {
                this.nextRound();
            }, 5000);
        });
    }

    unsubscribeListener(user, event){
        user.socket.removeAllListeners(event);
    }

    sendUsers(event: string, data: any = null){
        this.users.array.forEach(user => {
            user.con.emit(event, data);
        })
    }
    updateCzar(){
        this.czarIndex++;
        if(this.czarIndex >= this.users.length){
            this.czarIndex = 0;
        }
        this.czar = this.users.get(this.czarIndex);
        this.sendUsers("czar", this.czar.toLegacy());
    }

    checkBeforeStart(){
        
    }

    updateUser(user: ServerUser){
        if(user == null && user == undefined && user.con == undefined)
            return;
        user.con.emit("users", this.getLegacyUsers());
        user.con.emit("properties", this.properties);
        user.con.emit("gamestate", this.gameState);
        user.con.emit("started");
        user.con.emit("remaining", this.users.length - 1 - this.choice.size());
        user.con.emit("cards black", this.blackCard);
        user.con.emit("cards white", user.cards);
        user.con.emit("self", user.toLegacy());
        user.con.emit("choices", this.getLegacyChoices());
        if(this.czar != null)
            user.con.emit("czar", this.czar.toLegacy());
        user.con.emit("msgs", this.messages);

        this.listenCardChoice(user);
        this.listenCzarChoice(user);

        
    }

    configureUsers(){
        for(var u = 0; u < this.users.length; u++){
            var user = this.users.get(u)
            for(var i = 1; i <= this.properties.whiteCardAmount; i++){
                var index = Math.floor(Math.random() * this.whiteCards.length);
                var card = this.whiteCards.get(index);
                this.whiteCards.removeAt(index);
                user.giveCard(card);
            }
        }
        this.sendCards();
    }

    updatePlayerInfo(con: Connection){
        if(this.gameState == GameState.LOBBY)
            return;
        var user = this.getUser(con.token);
        this.logger.info("User tries to reconnect...");
        if(user){
            this.updateUser(user);
        }
    }

    /*
        CHAT
    */

    addMessage(msg: Message){
        this.messages.push(msg);
        this.sendUsers("chat", msg);
    }

    getLegacyChoices(): Map<LegacyUser, List<string>>{
        var map = new HashMap<LegacyUser, List<string>>();
        for(var i = 0; i < this.choice.size(); i++){
            var sUser = this.choice.map.get(i).key;
            map.push(this.getUser(sUser).toLegacy(), this.choice.map.get(i).value);
        }
        return map;
    }

    getLegacyUsers(): LegacyUser[]{
        var map: LegacyUser[]  = [];
        for(var i = 0; i < this.users.length; i++){
            var sUser = this.users.get(i);
            if(!sUser.toLegacy) continue;
            map.push(sUser.toLegacy());
        }
        return map;
    }

    getTestUser(): any[]{
        var map: any[]  = [];
        for(var i = 0; i < this.users.length; i++){
            var sUser = this.users.get(i);
            map.push(sUser.con.token);
        }
        return map;
    }

    getUser(id): ServerUser{
        for(var i = 0; i < this.users.length; i++){
            var user = this.users.get(i)
            if(user.con.token == id){
                return user;
            }
        }
    }

    containsConnection(con: Connection): boolean{
        for(var i = 0; i < this.users.length; i++){
            if(this.users.get(i).con.token == con.token)
                return true;
        }
        return false;
    }

    randomizeSelections(){
        var old: HashMap<string, List<string>> = this.choice.clone();
        var tmp: HashMap<string, List<string>> = new HashMap<string, List<string>>();
        for(var i = 0; i < this.choice.size(); i++){
            var random = Math.max(0, Math.floor(Math.random() * old.size()) - 1);
            this.logger.info("R: " + random)
            var pair = old.getByIndex(random)
            this.logger.info("R: " + JSON.stringify(pair))
            tmp.push(pair.key, pair.value);
            this.logger.info("R: " + tmp.size())
            old.remove(pair.key)
            this.logger.info("R: " + old.size())
        }
        this.choice = tmp;
    }

    public destroy(){
        this.logger.info("No players in gameroom\"" + this.ID + "\". Destroying game...");
        this.destroied = true;
        this.sendUsers("closed");
        games.remove(this);
        gameRemoved.next(this);
    }
    

    public toLegacyGame(){
        return {
            ID: this.ID,
            users: this.getLegacyUsers(),
            gameState: this.gameState,
            properties: this.properties
        }
    }

    
}

export enum GameState{
    SELECTING_WHITE,
    SELECTING_CZAR,
    WAITING,
    LOBBY
}

export class Properties{

    constructor(
        public maxPlayers: number = 8,
        public pointsToWin: number = 8,
        public cardSet: Array<string> = [
            "Base Set"
        ],
        public timeToSet: number = -1,
        public whiteCardAmount: number = 6,
        public blankCards: number = 0
    ){}
}

export interface User{
    cards: List<string>;
    name: string;
    token: string;
    score: number;

}

export class LegacyUser implements User{
    cards: List<string>;
    name: string;
    token: string
    isHost: boolean;
    constructor(
        name: string,
        token: string,
        isHost: boolean,
        cards: List<string> = new ArrayList<string>(),
        public score: number = 0
    ){
        this.name = name;
        this.token = token;
        this.isHost = isHost;
        this.cards = cards;
    }
}
export class ServerUser implements User{
    cards: List<string>;
    isHost: boolean;
    constructor(
        public con: Connection, 
        isHost: boolean,
        cards: List<string> = new ArrayList<string>(),
        public score: number = 0
    ){
        this.isHost = isHost;
        this.cards = cards;
    }

    giveCard(card: string){
        this.cards.add(card);
    }

    wonRound(){
        this.score += 1;
    }
    toLegacy(): LegacyUser{
        return new LegacyUser(this.name, this.con.token, this.isHost, this.cards, this.score);
    }

    get name(){
        return this.con.nickname;
    }

    get token(){
        return this.con.token;
    }
}

export class BlackCard{
    constructor( public content: string, public pick: number = 1 ){}
}


export class Set{

    constructor(
        public blackCards: Array<BlackCard> = [],
        public whiteCards: Array<string> = [],
        public name: string = "",
        public description: string = "No description",
        public official: boolean = false,
        public category: string = ""
    ){
    }

    public toLegacy(): LegacySet{
        return new LegacySet(this.name, this.description, this.official, this.whiteCards.length, this.blackCards.length, this.category);
    }
    
}

export class LegacySet{
    constructor(
        public name: string = "",
        public description: string = "No description",
        public official: boolean = false,
        public whiteCards: number = 0,
        public blackCards: number = 0,
        public category: string = ""
    ){
    }
}

export class Message{
    constructor(
      public user: string,
      public text: string
    ){}
  }