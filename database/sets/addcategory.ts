
import input from "input";
import * as fs from "fs";

fs.readdir("./_prepared/", (err, files) => {
    run(files).then(() => {
        console.log("done");
    })
})

async function run(files){
    for(let f of files){
        await addCategory(f);
    }
}


async function addCategory(file: any){

    var data;

    try{
        data = JSON.parse(fs.readFileSync("./_prepared/" + file, {encoding: "utf8"}));
    }catch(err){
        console.error("Could not parse file " + file);
        console.error(err);
        return;
    }

    console.log(`${data.name}; official: ${data.true}; "${data.description}"; B:${data.blackCards.length}; W:${data.whiteCards.length}`)
    const category = await input.text('Name the category', { default: 'other' });
    data.category = category;

    fs.writeFileSync("./_prepared/" + file, JSON.stringify(data), {encoding: "utf8"});

}