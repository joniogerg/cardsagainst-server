const fs = require("fs");


if(process.argv.length <= 2){
    console.log("You must specify a folder for the setup!");
    process.exit(1);
}

var folder = process.argv[2];



var processCounter = 0;

if(folder == "-a"){
    fs.readdir("./", (err, files) => {
        for(let dir of files){
            if(dir != "setup.js" && dir != "setup.js.map" && dir != "setup.ts" && dir != "default.json" && dir != "_prepared"){
                console.log("Prepairing " + dir);
                prepareSet(dir);
            }
        }
    })
}else{
    prepareSet(folder);
}

function prepareSet(folder){

    var pack = {
        name: "",
        official: false,
        description: "",
        blackCards: [],
        whiteCards: []
    }

    fs.readFile("./" + folder + "/black.html.txt", "utf8", (err, file) => {
        if(err) throw err;
    
        for(let string of file.split("\n")){
            if(string == "")
                continue;
    
            var pick = countChar(string, "_");
    
            if(pick == 0)
                pick = 1;
    
            pack.blackCards.push({
                content: string, 
                pick: pick
            })
        }
    
        finished();
    });
    
    fs.readFile("./" + folder + "/white.html.txt", "utf8", (err, file) => {
        if(err) throw err;
    
        for(let string of file.split("\n")){
            if(string == "")
                continue
            pack.whiteCards.push(string)
        }
    
        finished();
    });
    
    fs.readFile("./" + folder + "/metadata.json", "utf8", (err, file) => {
        if(err) throw err;
    
        var metadata = JSON.parse(file);
        pack.name = metadata.name
        pack.description = metadata.description
        if(pack.description == "- placeholder -"){
            pack.description = "No description";
        }
        pack.official = metadata.official;
    
        finished();
    });
    
    
    function finished(){
        processCounter++;
        if(processCounter >= 3){

            var data = JSON.stringify(pack);
    
            fs.writeFile("./_prepared/" + folder + ".json", data,  (err) => {
                if(err) throw err;
            })

        }
    }
    
    function countChar(string: string, char): number{
        var counter = 0;
    
        for(var i = 0; i < string.length; i++){
            if(char == string.charAt(i)){
                counter++;
            }
        }
    
        return counter;
    }


}
