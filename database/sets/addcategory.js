"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const input_1 = require("input");
const fs = require("fs");
fs.readdir("./_prepared/", (err, files) => {
    run(files).then(() => {
        console.log("done");
    });
});
function run(files) {
    return __awaiter(this, void 0, void 0, function* () {
        for (let f of files) {
            yield addCategory(f);
        }
    });
}
function addCategory(file) {
    return __awaiter(this, void 0, void 0, function* () {
        var data;
        try {
            data = JSON.parse(fs.readFileSync("./_prepared/" + file, { encoding: "utf8" }));
        }
        catch (err) {
            console.error("Could not parse file " + file);
            console.error(err);
            return;
        }
        console.log(`${data.name}; official: ${data.true}; "${data.description}"; B:${data.blackCards.length}; W:${data.whiteCards.length}`);
        const category = yield input_1.default.text('Name the category', { default: 'other' });
        data.category = category;
        fs.writeFileSync("./_prepared/" + file, JSON.stringify(data), { encoding: "utf8" });
    });
}
//# sourceMappingURL=addcategory.js.map