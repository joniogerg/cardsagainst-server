"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const queue_1 = require("./queue");
const mysql = require("mysql");
const jwt = require("jsonwebtoken");
const uuid = require("uuid/v4");
const winston = require("winston");
const { combine, timestamp, label, printf } = winston.format;
const SECRET = (Math.floor(Math.random() * 100000) / 100000) + "";
var con = null;
var THREADHOLD = 6;
var logger = null;
class Auth {
    constructor(format, webDebug) {
        this.format = format;
        this.webDebug = webDebug;
        this.guestQueue = new queue_1.Queue((username, resolve, reject) => {
            con.query("SELECT * FROM `guests` WHERE USERNAME='" + username + "'", function (err, result) {
                if (err) {
                    logger.error(err);
                }
                if (result.length == 0) {
                    var token = jwt.sign({ username: username }, SECRET);
                    con.query("INSERT INTO `guests` (`USERNAME`, `TOKEN`) VALUES ('" + username + "', '" + token + "')");
                    resolve(token);
                }
                else {
                    reject(username);
                }
            });
        }, 3, false);
        logger = winston.createLogger({
            format: combine(label({ label: "AUTH" }), timestamp(), format),
            transports: [
                new winston.transports.Console(),
                new winston.transports.File({
                    filename: "./logs/auth.log"
                }),
                webDebug
            ]
        });
        this.connect().then(() => {
            logger.info("Connected to database");
            this.cleanup();
        }).catch(err => {
            logger.error("Error occured while connecting to the mysql database");
            logger.error(err);
        });
    }
    guest(username) {
        if (username.length > 20) {
            username = username.substring(0, 20);
        }
        return this.guestQueue.add(username);
    }
    isLoggedIn(token) {
        return new Promise((resolve, reject) => {
            con.query("SELECT * FROM `guests` WHERE TOKEN = '" + token + "'", function (err, result) {
                if (err)
                    throw err;
                if (result.length == 0) {
                    resolve({ success: false });
                }
                else {
                    resolve({ success: true, username: result[0]["USERNAME"] });
                }
            });
        });
    }
    login(username, password) {
    }
    logout(token) {
        logger.info("User logged out: " + jwt.decode(token, SECRET).username);
        con.query("DELETE FROM `guests` WHERE `guests`.`TOKEN` = '" + token + "'");
    }
    connect() {
        return new Promise((resolve, reject) => {
            THREADHOLD--;
            con = mysql.createPool({
                host: "localhost",
                user: "cardsagainst",
                password: "df6xC7V2jXJm8Pd2",
                database: "cardsagainst",
                connectionLimit: 10,
                waitForConnections: true
            });
            resolve();
        });
    }
    cleanup() {
        con.query("TRUNCATE `guests`");
        logger.info("Cleaned guest database up");
    }
}
exports.Auth = Auth;
function connect(finished) {
}
//# sourceMappingURL=auth.js.map