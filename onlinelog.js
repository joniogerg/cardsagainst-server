"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = require("winston");
const util_1 = require("./util");
class OnlineLogger extends winston_1.Transport {
    constructor(opts) {
        super(opts);
        this.sockets = new util_1.ArrayList();
        //
        // Consume any custom options here. e.g.:
        // - Connection information for databases
        // - Authentication information for APIs (e.g. loggly, papertrail, 
        //   logentries, etc.).
        //
    }
    log(info, callback) {
        this.sockets.array.forEach(socket => {
            socket.emit("add log", info);
        });
        callback();
    }
    addSocket(socket) {
        this.sockets.add(socket);
    }
    removeSocket(socket) {
        this.sockets.remove(socket);
    }
}
exports.OnlineLogger = OnlineLogger;
class ConnectionLogger extends winston_1.Transport {
    constructor(opts) {
        super(opts);
        this.sockets = new util_1.ArrayList();
        //
        // Consume any custom options here. e.g.:
        // - Connection information for databases
        // - Authentication information for APIs (e.g. loggly, papertrail, 
        //   logentries, etc.).
        //
    }
    log(info, callback = null) {
        this.sockets.array.forEach(socket => {
            socket.emit("add log connection", info);
        });
        if (callback)
            callback();
    }
    addSocket(socket) {
        this.sockets.add(socket);
    }
    removeSocket(socket) {
        this.sockets.remove(socket);
    }
}
exports.ConnectionLogger = ConnectionLogger;
//# sourceMappingURL=onlinelog.js.map