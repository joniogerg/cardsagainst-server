/* Copyright walamana.de 2018 */


require('source-map-support').install();

import { WebDebug } from "./webdebug";

var version = 20;
var versionCode = "0.9.6 ALPHA"

var changelog = [
    "Added categories for the set list",
    "I'm currently refactoring most of the code, so there might (probably <i>will</i>) be some issues while playing!",
    "<strong>If ever something weird happens or if an error occures, please report it to <a href='https://gitlab.com/joniogerg/cardsagainst-server/issues'>this page</a>. Thank you!</strong>"
]
 
 
const winston = require("winston");
const { combine, timestamp, label, printf } = winston.format;

const myFormat = printf(info => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

const webDebug = new WebDebug({
    handleExceptions: true
})


const logger = winston.createLogger({
    format: combine(
        label({ label: 'CAR' }),
        timestamp(),
        myFormat
    ),
    transports: [
        new winston.transports.Console({
            handleExceptions: true
        }),
        new winston.transports.File({
            filename: "logs.log",
            handleExceptions: true
        }),
        webDebug
    ],
    exitOnError: false
});

logger.info("Startup 1");
const fs = require("fs");

import game = require("./game");
game.webDebug = webDebug;
game.gLogger = logger;
game.format = myFormat;
import { ArrayList, HashMap } from "./util";
import { GameRoom, GameState, gameAdded } from "./game";
import { Connection, ConnectionHandler } from "./connectionHandler";
import { Auth } from "./auth";

const auth = new Auth(myFormat, webDebug);
logger.info("Startup 2");

const CATEGORY_ORDER = ["essential", "people", "topic", "social", "nostalgia", "tv", "pax", "other"]


const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http);

// app.get("/:id", (req, resp) => {
//     fs.readFile(__dirname + "/onlinelog.html", "utf8", (err, data) => {
//         resp.send(data.replace("%%id%%", req.params.id));
//     });
// });

app.get("/", (req, resp) => {
    resp.send("API is running")
});

app.get("/debug", (req, resp) => {
    resp.sendFile(__dirname + "/webdebug.html");
});

http.listen(process.env.PORT, () => {
    logger.info("Server listening on *:" + process.env.PORT);
})
logger.info("Startup 3");


let disconnectTimeout = new HashMap<string, any>();
const TIMEOUT = 10000;


const connectionHandler = new ConnectionHandler(io, auth, myFormat, webDebug);

game.loadSets();
logger.info("Startup 4");

connectionHandler.on("connection").subscribe(con => {

    var curGame: GameRoom = null;
    var gameAddedSubscription = null;
    var gameRemovedSubscription = null;
    var gameChangedSubscription = null;

    logger.info(con.socket.id);

    
    // Handles reconnections

    con.on("reconnect", (socket) => {
        con.emit("id", con.socket.id)
        con.emit("version", {
            version: version,
            versionCode: versionCode
        })
        con.emit("changelog", changelog);

        if(curGame != null){
            if(curGame.gameState == GameState.LOBBY){
                curGame.leave(con);
            }
            curGame.updatePlayerInfo(con);
        }
    });

    con.emit("id", con.socket.id)
    con.emit("version", {
        version: version,
        versionCode: versionCode
    })
    con.emit("changelog", changelog);

    if(curGame != null){
        curGame.updatePlayerInfo(con);
    }

    con.on("create", function(properties){
        game.createNewRoom((properties.id != null ? properties.id.substring(0,5) : properties.id), properties.props, con, webDebug).then(gameRoom => {
            if(gameRoom == null){
                con.emit("err", {code: 3, message: "The sets have not been fully loaded yet. Please wait!"});
                return;
            }
            con.emit("created", gameRoom.ID)}
        );
    });

    con.on("join", (req) => {
        if(game.getRoomByID(req.id) != null && req.nickname != null ){
            curGame = game.getRoomByID(req.id);
            curGame.join(con);

            if(gameAddedSubscription != null)
                con.subscriptions.remove(gameAddedSubscription);

        }else{
            con.emit("err", {code: 2, message: "Game doesn't exist!", data: req.id})
        }
        
    });

    con.on("logout", (token) => {
        con.destroyConnection();
    });

    con.on("home", () => {
        if(curGame != null){
            curGame.leave(con); 
            curGame = null;
        }

        

        // con.emit("sets", getSets());
    });


    con.on("get sets categorised", () => {
        logger.info("Sets requested categorised");
        var categories = {};
        for(const set of getSets()){
            if(Object.keys(categories).indexOf(set.category) == -1){
                categories[set.category] = [];
            }
            categories[set.category].push(set);
        }
        logger.info(Object.keys(categories));
        con.emit("sets", categories);
    });

    con.on("get sets", () => {
        logger.info("Sets requested");
        con.emit("sets", getSets());
    });

    con.on("get games", () => {
        con.emit("games", game.getLegacyGames().array);

            
        if(gameAddedSubscription == null)
            gameAddedSubscription = game.gameAdded.subscribe(gameroom => {
                con.emit("games added", gameroom.toLegacyGame())
            })

        if(gameRemovedSubscription == null)
            gameRemovedSubscription = game.gameRemoved.subscribe(gameroom => {
                con.emit("games removed", gameroom.ID)
            })

        if(gameChangedSubscription == null)
            gameChangedSubscription = game.gameChanged.subscribe(gameroom => {
                con.emit("games changed", gameroom.toLegacyGame())
            })

        con.subscriptions.add(gameAddedSubscription);
        con.subscriptions.add(gameRemovedSubscription);
    })

    con.on("disconnect", () => {
        if(this.curGame != null && this.curGame.gameState == GameState.LOBBY){
            this.curGame.leave(con);
        }
    })

    
    
    
    con.on("destroy", () => {
        if(curGame != null){
            curGame.leave(con);
        }
    });

 
});


function getSets(){
    var sets = game.getLegacySets();
    logger.info("Categories: " + game.getLegacySets().map(v => v.name).join(", "));
    sets.sort((a, b) => {
        var iA = CATEGORY_ORDER.indexOf(a.category);
        var iB = CATEGORY_ORDER.indexOf(b.category)
        return (iA < iB) ? -1 : (iA > iB ? 1 : 0);
    })
    return sets;
}

/* 

        ERRORS:

    Auth:
    1 Username already in use

    Lobby:
    2 Game does not exist

*/